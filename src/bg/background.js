const DEBUG_LIMIT = 50
var cache = {};
var api_count = 0;

var warning = ["China", "Iran", "Russia"];

function getcountry(origin_ip, ip_hop){
    if (api_count > DEBUG_LIMIT){
        console.log("API debug-limit reached");
        return null;
    }
    var request = new XMLHttpRequest();
    request.open('GET', "http://api.ipstack.com/"+ip_hop+"?access_key=8d1a616122d5d3e79a7a38aedb341307", true);
    request.onload = function () {
              var data = JSON.parse(this.response);
              if (data.ip) { // make sure the response is valid
                if (!cache[origin_ip]) cache[origin_ip] = new Set();
                
		if (!cache[origin_ip].has(data.country_name)) {
			cache[origin_ip].add(data.country_name); // country might be null (should be cached too)

                	maybe_alert_user(data.country_name);
		}
              } else {
                console.log("ERROR: " + this.response);
              }
            }
    request.send();
    api_count += 1;
}

function maybe_alert_user(country){
    if (warning.indexOf(country) > -1) {

        chrome.notifications.create('', {
             type: 'basic',
             iconUrl: '../../icons/icon128.png',
             title: 'Warning!',
             message: 'your traffic went through: ' +country
        }, function(notificationId) {});
    }
}


function trout(ip_) {
    getcountry(ip_, ip_);
    var request = new XMLHttpRequest();
    request.open('GET', "http://127.0.0.1:3000/"+ip_, true);
    request.onload = function () {
          if (this.response){
              var data = JSON.parse(this.response);
              for (var i = 0; i < data.length; i++){
                    var ip_hop = Object.keys(data[i])[0];
                    if (ip_hop) getcountry(ip_, ip_hop);
              }
          }
    }
    request.send();
}



chrome.webRequest.onCompleted.addListener((details) => {

        var size = Object.keys(cache).length;
        if (size > 1000) {
            cache = {};
        }
        var check = details.url.indexOf("api.ipstack.com") == -1;
        var check2 = details.url.indexOf("127.0.0.1") == -1;
        var check3 = details.ip != "undefined";

        if ((!cache[details.ip]) && details.ip && check && check2 && check3)
            trout(details.ip);

        if (cache[details.ip]){
           var countries_ = Array.from(cache[details.ip]);
           console.log(String(size) + ") "+ details.ip+" => "+countries_.join(','));
        }

    },{urls: ["<all_urls>"]});


chrome.runtime.onMessage.addListener( function(request,sender,sendResponse){
    warning.push(request.new_country)
});
